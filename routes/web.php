<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');

Route::get('/all-products', 'ProductsController@getAll');

Route::group(['middleware'=>'auth','prefix'=>'admin'], function (){
    Route::get('products/select-category', 'Admin\\ProductsController@selectCategory')->name('select-category');
    Route::resource('categories','Admin\\CategoriesController');
    Route::resource('attributes','Admin\\AttributesController');
    Route::resource('products','Admin\\ProductsController');
});


Route::get('/','ProductsController@index');
Route::get('all-products','ProductsController@index')->name('all-products');
Route::get('/product/{product_id}/{slug}','ProductsController@detail');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
