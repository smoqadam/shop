<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Attribute extends Model
{
    protected $fillable = [
        'title', 'description', 'category_id',
    ];

    public function products()
    {
        return $this->belongsToMany(Product::class, 'product_attribute')->withPivot('value_id');
    }

    public function values()
    {
        return $this->hasMany(Value::class);
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function getValues($productId, $attributeId)
    {
        return DB::table('attributes')
            ->leftJoin('product_attribute', 'product_attribute.attribute_id','=','attributes.id')
            ->leftJoin('values', 'product_attribute.value_id','=','values.id')
            ->where('product_attribute.product_id','=',$productId)
            ->where('product_attribute.attribute_id','=',$attributeId)
            ->get();
    }

}
