<?php

namespace App\Http\Controllers;

use App\Attribute;
use App\Category;
use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProductsController extends Controller
{
    public function index()
    {
        $products = Product::orderby('id', 'desc')->paginate(12);

        return view('frontend.products.index', [
            'products' => $products,
            'categories' => Category::all(),
            'attributes' => Attribute::all(),
        ]);
    }

    public function detail($product_id, $slug, Request $request)
    {
        $product = Product::find($product_id);

        return view('frontend.products.detail', [
            'product' => $product,
        ]);
    }

    public function filter(Request $request)
    {
        $attributes = $request->get('attributes', []);
        $categories = $request->get('categories', []);

        if (empty($attributes) and empty($categories)) {
            $allProduct = Product::paginate(10);
        } else {
            $allProduct = DB::table('products')->
            join('product_attribute', 'product_attribute.product_id', 'products.id')->
            join('values', 'values.id', '=', 'product_attribute.value_id')->
            whereIn('products.category_id', $categories)->
            orWhereIn('values.id', $attributes)->
            groupBy('products.id')->get();
        }

        return view('frontend.products.product', [
            'products' => $allProduct,
        ]);
    }
}
