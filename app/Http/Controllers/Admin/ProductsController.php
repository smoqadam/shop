<?php

namespace App\Http\Controllers\Admin;

use App\Attribute;
use App\Category;
use App\Product;
use App\Value;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::paginate(20);

        return view('admin.products.index', [
            'products' => $products,
        ]);
    }

    public function selectCategory()
    {
        return view('admin.products.select-category', [
            'categories' => Category::where('has_children', false)->get(),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param $request Request
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $categoryId = $request->get('category_id');

        $attributes = Attribute::where('category_id', $categoryId)->get();
        return view('admin.products.create', [
            'attributes' => $attributes,
            'categories' => Category::where('has_children', false)->get(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $attributesValues = $request->get('values');
        $file = $request->file('imgUrl');
        $imgUrl = $file->storeAs(public_path('images'), $file->getClientOriginalName());

        $product = new Product();
        $product->title = $request->get('title');
        $product->description = $request->get('description');
        $product->quantity = $request->get('quantity');
        $product->status = $request->get('status');
        $product->price = $request->get('price');
        $product->category_id = $request->get('category_id');
        $product->imgUrl = $imgUrl;
        $product->save();

        foreach ($attributesValues as $attributeId => $attributesValue) {
            foreach ($attributesValue as $value) {
                $value = new Value([
                    'title' => $value,
                    'product_id' => $product->id,
                ]);
                $insertedValue = Attribute::find($attributeId)->values()->save($value)->id;
                $valueIds[$attributeId][] = ['value_id' => $insertedValue];
                $product->attributes()->attach([$attributeId => ['value_id' => $insertedValue]]);
            }
        }

        return redirect(route('products.index'))->with('success', 'Successfully saved');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = Product::find($id);
        $attributes = $product->attributes()->groupBy('product_attribute.attribute_id')->get();
        return view('admin.products.edit', [
            'product' => $product,
            'attributes' => $attributes,
            'categories' => Category::where('has_children', false)->get(),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $attributesValues = $request->get('values');
        $product = Product::findOrFail($id);
        Value::where('product_id', $product->id)->delete();
        $product->attributes()->detach();
        foreach ($attributesValues as $attributeId => $attributesValue) {
            foreach ($attributesValue as $value) {
                $value = new Value([
                    'title' => $value,
                    'product_id' => $product->id,
                ]);
                $insertedValue = Attribute::find($attributeId)->values()->save($value)->id;
                $valueIds[$attributeId][] = ['value_id' => $insertedValue];
                $product->attributes()->attach([$attributeId => ['value_id' => $insertedValue]]);
            }
        }


        return redirect(route('products.index'))->with('success', 'Successfully saved');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::findOrFail($id);
        $product->attributes()->detach();
        $product->delete();

        return response()->json(['success' => 'Successfully deleted']);
    }
}
