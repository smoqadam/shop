<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.categories.index', [
            'categories' => Category::all(),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Category::create($request->except('_token'));
        if ($request->get('parent_id')) {
            $parent = Category::find($request->get('parent_id'));
            $parent->has_children = true;
            $parent->save();
        }

        return redirect(route('categories.index'))->with('success', 'Successfully updated');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = Category::findOrFail($id);

        return view('admin.categories.index', [
            'category' => $category,
            'categories' => Category::all(),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $category = Category::findOrFail($id);
        $category->title = $request->get('title');
        $category->description = $request->get('description');
        $category->parent_id = $request->get('parent_id');
        $category->save();

        if ($request->get('parent_id')) {
            $parent = Category::find($request->get('parent_id'));
            $parent->has_children = true;
            $parent->save();
        }

        return redirect(route('categories.index'))->with('success', 'Successfully updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            Category::find($id)->delete();
        } catch (QueryException $exception) {
            return response()->json(['error'=>'This category has some attributes. please update them before delete the category'],422);
        }

        return response()->json(['success']);
    }
}
