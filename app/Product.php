<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{

    protected $fillable = [
        'title','category_id','status','description','imageUrl','price','quantity'
    ];

    public function attributes()
    {
        return $this->belongsToMany(Attribute::class,'product_attribute')->withPivot('value_id');
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public static function getStatuses()
    {
        return [
            'Not Available'=>'Not Available',
            'Available'=>'Available',
            'Coming Soon'=>'Coming Soon',
        ];
    }
}
