<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Value extends Model
{

    protected $fillable = [
        'title','product_id'
    ];

    public function attribute()
    {
        return $this->belongsTo(Attribute::class);
    }

    public function product()
    {
        return $this->belongsTo(Product::class);
    }
}
