@extends('admin.layouts.main')

@section('content')
    <div class="row">
        <div class="col-md-12">

            <h1 class="page-header">
                Products
                <a href="{{ route('select-category') }}" class="btn btn-success">
                    <i class="glyphicon glyphicon-plus"></i> Add new product
                </a>
            </h1>
            @if(session()->has('success'))
                <div class="alert alert-success">
                    {{ session()->get('success') }}
                </div>
            @endif

            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Title</th>
                        <th>Price</th>
                        <th>Quantity</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($products as $product)
                        <tr>
                            <td>{{ $product->id }}</td>
                            <td>{{ $product->title }}</td>
                            <td>{{ $product->price }}</td>
                            <td>{{ $product->quantity }}</td>
                            <td>{{ $product->status }}</td>
                            <td>
                                <a href="{{ route('products.edit',[$product]) }}" ><i class="glyphicon glyphicon-edit"></i></a>
                                <a href="{{ route('products.destroy',[$product]) }}" class="delete-product"><i class="glyphicon glyphicon-trash"></i></a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@stop

@section('scripts')
    <script>
        $(function () {
            $('.delete-product').click(function(e){
                e.preventDefault();
                var url = $(this).attr('href');

                var $this = $(this);
                $.ajax({
                    url:url,
                    type:'DELETE',
                    data:{_token:'{{ csrf_token() }}'},
                    success: function(data){
                        console.log(data);
                        $this.parent().parent().hide('slow');
                    }
                })
            })
        })
    </script>
@stop