@extends('admin.layouts.main')

@section('content')
    <h2 class="page-header">
        Select Product Category
    </h2>
    <p>
        Before adding a new product please select a category:
    </p>
    <form action="{{ route('products.create') }}">
        <label for="category"></label>
        <select name="category_id"  class="form-control categories" id="category">
            <option value="0">----</option>
            @foreach($categories as $category)
                <option value="{{ $category->id }}">{{ $category->title }}</option>
            @endforeach
        </select>
        <br>
        <input type="submit" value="Continue" class="btn btn-success pull-right" />
    </form>
@stop