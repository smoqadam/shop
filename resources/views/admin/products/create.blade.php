@extends('admin.layouts.main')
@section('content')
    <div class="col-md-9">

        <h1 class="page-header">
            Add new product
        </h1>
    <form action="{{ route('products.store') }}" enctype="multipart/form-data" method="post">
        {{ csrf_field() }}
        <div class="row">
            <div class="form-group ">
                <label class="col-md-12" for="title">Title</label>
                <input class="form-control" type="text" name="title" value="{{ $product->title  or old('title') }}">
            </div>
        </div>
        <div class="row">
            <div class="form-group ">
                <label class="col-md-12" for="desc">Description</label>
                <textarea name="description" class="form-control" id="desc" cols="30" rows="10">{{ $product->description  or old('description') }}</textarea>
            </div>
        </div>

        <div class="row">
            <div class="form-group ">
                <label class="col-md-12" for="status">Status</label>
                <select name="status" id="status" class="form-control">
                    <option value="0">----</option>
                    @foreach(\App\Product::getStatuses() as $value => $status)
                        <option value="{{ $value }}" {{  $product->status or old('status') == $value ? 'selected':''}}>
                            {{ $status }}
                        </option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="row">
            <div class="form-group ">
                <label class="col-md-12" for="category">Category</label>
                    <select name="category_id" id="category" class="form-control" >
                        <option value="0">----</option>
                        @foreach($categories as $category)
                            <option value="{{ $category->id}}"
                                    {{ request('category_id') == $category->id ? 'selected' :'' }}>
                                {{ $category->title }}
                            </option>
                        @endforeach
                    </select>
                <a href="{{ route('select-category') }}" class="btn btn-info">Select another category</a>
            </div>
        </div>
        <br>

        <div class="row">
            <div class="form-group ">
                <label class="col-md-12" for="img">Image</label>
                <input type="file" name="imgUrl" id="img" class="form-control">
            </div>
        </div>
        <div class="row">
            <div class="form-group ">
                <label class="col-md-12" for="quantity">Quantity</label>
                <input type="text" id="quantity" value="{{ $product->quantity  or old('quantity') }}" name="quantity" >
            </div>
        </div>

        <div class="row">
            <div class="form-group ">
                <label class="col-md-12" for="price">Price</label>
                <input class="form-control" id="price" type="text" value="{{ $product->price  or old('price') }}" name="price" >
            </div>
        </div>
        <div class="row">
            <div class="form-group ">
                <h3 class="col-md-12">Attributes</h3>
                <div class="attributes">
                    @foreach($attributes as $attribute)
                        <label for="">
                            {{ $attribute->title }}
                        </label>
                        <select multiple="multiple" name="values[{{$attribute->id}}][]" id="" class="value-attributes form-control">
                            {{--@foreach($attribute->values as $value)--}}
                                    {{--<option value="{{$value->id}}">{{$value->title}}</option>--}}
                            {{--@endforeach--}}
                        </select>
                    @endforeach
                </div>
            </div>
        </div>
        <input class="btn btn-success" type="submit" value="save">
    </form>
    </div>
@stop



@section('scripts')
    <script>
        $(function () {
            $(".value-attributes").select2({
                tags: true,
            });
        });
    </script>
@stop