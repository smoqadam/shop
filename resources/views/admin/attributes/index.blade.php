@extends('admin.layouts.main')

@section('content')


    <div class="row">
        <div class="col-md-12">

            <h1 class="page-header">
                Attributes
            </h1>
            @if(session()->has('success'))
                <div class="alert alert-success">
                    {{ session()->get('success') }}
                </div>
            @endif
            <div class="col-md-5">
                <form action="{{ isset($attribute) ? route('attributes.update',[$attribute]) : route('attributes.store') }} " method="post">
                    @if(isset($attribute))
                        {{ method_field('PATCH') }}
                    @endif

                    {{ csrf_field() }}
                    <div class="row">
                        <div class="form-group ">
                            <label class="col-md-12" for="title">Title</label>
                            <input class="form-control" type="text" value="{{ $attribute->title or old('title') }}" name="title">
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group  ">
                            <label for="desc" class="col-md-12">Description</label>
                            <textarea name="description" class="form-control" id="desc" cols="30" rows="10">{{ $attribute->description or old('description') }}</textarea>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group ">
                            <label for="category_id" class="col-md-12">Category</label>
                            <select name="category_id" class="form-control" id="category_id">
                                <option value="0">----</option>
                                @foreach($categories as $category)
                                    <option value="{{ $category->id }}"
                                    >{{ $category->title }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <input class="btn btn-success" type="submit" value="save">
                </form>
            </div>
            <div class="col-md-7">
                <table class="table">
                    <thead>
                        <tr>
                            <th>Title</th>
                            <th>Description</th>
                            <th>Category</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($attributes as $attribute)
                            <tr>
                                <td>{{ $attribute->title }}</td>
                                <td>{{ $attribute->description }}</td>
                                <td>{{ $attribute->category->title }}</td>
                                <td>
                                    <a href="{{ route('attributes.edit',[$attribute]) }}"><i class="glyphicon glyphicon-edit"></i></a>
                                    <a class="delete-attr" href="{{ route('attributes.destroy',[$attribute]) }}"><i class="glyphicon glyphicon-trash"></i></a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@stop


@section('scripts')

    <script>
        $(function () {
            $('.delete-attr').click(function(e){
                e.preventDefault();
                var url = $(this).attr('href');

                var $this = $(this);
                $.ajax({
                    url:url,
                    type:'DELETE',
                    dataType:'json',
                    data:{_token:'{{ csrf_token() }}'},
                    success: function(){
                        $this.parent().parent().hide('slow');
                    },
                    error:function(data){
                        alert(data.responseJSON.error)
                    }
                })
            })
        })
    </script>
@stop