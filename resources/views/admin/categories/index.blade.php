@extends('admin.layouts.main')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <h1 class="page-header">
                Categories
            </h1>
            @if(session()->has('success'))
                <div class="alert alert-success">
                    {{ session()->get('success') }}
                </div>
            @endif
            <div class="col-md-5">
                <form action="{{ isset($category) ? route('categories.update',[$category]) : route('categories.store') }} " method="post">
                    {{ csrf_field() }}
                    @if(isset($category))
                        {{ method_field('PATCH') }}
                    @endif
                    <div class="row">
                        <div class="form-group ">
                            <label class="col-md-12" for="title">Title</label>
                            <input type="text" id="title" name="title" value="{{ $category->title or old('title') }}" class="form-control">
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group  ">
                            <label for="desc" class="col-md-12">Description</label>
                            <textarea name="description" class="form-control" id="desc" cols="30" rows="10">{{ $category->description or old('description') }}</textarea>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group ">
                            <label for="parent" class="col-md-12">Parent</label>
                            <select name="parent_id" class="form-control" id="parent">
                                <option value="0">----</option>
                                @foreach($categories as $category)
                                    <option value="{{ $category->id }}">{{ $category->title }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <input class="btn btn-success" type="submit" value="save">
                </form>
            </div>
            <div class="col-md-7">
                <table class="table">
                    <thead>
                        <tr>
                            <th>Title</th>
                            <th>Description</th>
                            <th>Parent</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($categories as $category)
                            <tr>
                                <td>{{ $category->title }}</td>
                                <td>{{ $category->description }}</td>
                                <td>{{ $category->parent_id }}</td>
                                <td>
                                    <a href="{{ route('categories.edit',[$category]) }}"><i class="glyphicon glyphicon-edit"></i></a>
                                    <a class="delete-cat" href="{{ route('categories.destroy',[$category]) }}"><i class="glyphicon glyphicon-trash"></i></a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@stop

@section('scripts')

    <script>
        $(function () {
            $('.delete-cat').click(function(e){
                e.preventDefault();
                var url = $(this).attr('href');

                var $this = $(this);
                $.ajax({
                    url:url,
                    type:'DELETE',
                    dataType:'json',
                    data:{_token:'{{ csrf_token() }}'},
                    success: function(){
                        $this.parent().parent().hide('slow');
                    },
                    error:function(data){
                        alert(data.responseJSON.error)
                    }
                })
            })
        })
    </script>
@stop