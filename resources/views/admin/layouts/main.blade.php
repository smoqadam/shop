<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.4/css/select2.min.css" rel="stylesheet" />
        <link rel="stylesheet" href="/css/app.css">
    <link rel="stylesheet" href="/css/jquery.tagit.css">
    <link rel="stylesheet" href="/css/tagit.ui-zendesk.css">
    <title>Laravel</title>
</head>
<body>
    <div class="row">
        <!-- Menu -->
        <div class="side-menu">
            <nav class="navbar navbar-default" role="navigation">
                <div class="side-menu-container">
                    <ul class="nav navbar-nav">
                        <li><a href="{{ route('categories.index') }}"><span class=""></span> Categories</a></li>
                        <li><a href="{{ route('attributes.index') }}"><span class=""></span>Attributes</a></li>
                        <li><a href="{{ route('products.index') }}"><span class=""></span> Products</a></li>
                    </ul>
                </div>
            </nav>
        </div>
        <!-- Main Content -->
        <div class="container-fluid">
            <div class="side-body">
                @yield('content')
            </div>
        </div>
    </div>
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.12/jquery-ui.min.js" type="text/javascript" charset="utf-8"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.4/js/select2.min.js"></script>
    <script src="/js/tag-it.js"></script>
    @yield('scripts')
</body>
</html>