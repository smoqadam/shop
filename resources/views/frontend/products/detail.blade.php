@extends('frontend.layouts.layout')

@section('content')
    <h2>{{ $product->title }}</h2>
    <p>{{ $product->description }}</p>
    <h3>Attributes</h3>
    @foreach($product->attributes()->groupBy('product_attribute.attribute_id')->get() as $attribute)
        {{ $attribute->title }}
        <ul>
            @foreach($attribute->getValues($product->id, $attribute->id) as $value)
                <li>{{ $value->title }}</li>
            @endforeach
        </ul>
    @endforeach
@stop