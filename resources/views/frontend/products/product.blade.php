@foreach($products as $product)
    <div class="col-sm-4">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <a href="/product/{{ $product->id }}/{{str_replace(' ','-',$product->title)}}">
                    {{ $product->title }}
                </a>
            </div>
            <div class="panel-body">
                <img src="{{ $product->imgUrl }}" class="img-responsive" style="width:100%" alt="Image">
            </div>
            <div class="panel-footer">
                {{ number_format($product->price) }} Toman <br>
                <a href="/cart" class="btn btn-success">Buy</a> <br>
            </div>
        </div>
    </div>
@endforeach
