@extends('frontend.layouts.layout')
@section('content')
    <div class="row">
        <div class="col-md-9 products-list">
            @include('frontend.products.product')
        </div>
        <div class="col-md-3">
            <nav class="sidebar">

                <h5 class="page-header">Categories:</h5>
                <ul class="list-unstyled">
                    @foreach($categories as $category)
                        <li>
                            <label for="category_{{$category->id}}">
                                <input type="checkbox" class="filter" data-type="category" value="{{ $category->id }}" id="category_{{$category->id}}">{{ $category->title }}
                            </label>
                        </li>
                    @endforeach
                </ul>
                <h4 class="page-header">Attributes:</h4>
                <ul class="list-unstyled">
                    @foreach($attributes as $attribute)
                        <li>
                            @if($attribute->values->count())
                                <ul class="list-unstyled"><h4>{{ $attribute->title }}</h4>
                                    @foreach($attribute->values as $value)
                                        <li>
                                            <label for="attribute_{{$value->id}}">
                                                <input type="checkbox" data-type="attribute" class="filter" id="attribute_{{$value->id}}" value="{{ $value->id }}">
                                                {{ $value->title }}
                                            </label>
                                        </li>
                                    @endforeach
                                </ul>
                            @endif
                        </li>
                    @endforeach
                </ul>
            </nav>
        </div>
    </div>
@stop